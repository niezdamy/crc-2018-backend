﻿using AutoMapper;
using CRC.Repository.Abstract;
using CRC.Repository.Models;
using CRC.Services.Abstract;
using CRC.Services.ViewModels;
using System.Collections.Generic;
using System.Linq;
using CRC.Repository.Enums;
using Microsoft.EntityFrameworkCore;

namespace CRC.Services.Services
{
    public class RequestService : IRequestService
    {
        private readonly IGenericRepository<Request> _requestRepository;
        private readonly IGenericRepository<ProvisionedPermission> _permissionRepository;

        public RequestService(IGenericRepository<Request> requestRepository, IGenericRepository<ProvisionedPermission> permissionRepository)
        {
            _requestRepository = requestRepository;
            _permissionRepository = permissionRepository;
        }

        public IEnumerable<ReadRequestViewModel> GetAllIRequests()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<ReadRequestViewModel> GetMyRequests(int userId)
        {
            var requests = _requestRepository.GetAll().Where(r => r.UserId == userId);
            return Mapper.Map<IEnumerable<Request>, IEnumerable<ReadRequestViewModel>>(requests);
        }      

        public int CreateNewRequest(CreateRequestViewModel requestVm)
        {
            var request = Mapper.Map<Request>(requestVm);
            request.Status = StatusEnum.InProgress; 
            return _requestRepository.Add(request);
        }

        public void Approve(int id)
        {

            //pobierz request po id z repozytorium
            //ustaw status
            //zapisz (edytuj encję)
            
            //stwórz nowy obiekt typu ProvisionedPermission *(można użyć automappera) na podstawie requesta
            //zapisz go do repozytorium permissionów
        }

        public void Reject(int id)
        {         
        }

        public void Delete(int id)
        {
            var request = _requestRepository.GetById(id);
            _requestRepository.Delete(request);
        }
    }
}
